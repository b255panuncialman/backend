/*
    Create functions which can manipulate our arrays.
*/

let registeredUsers = [

    "James Jeffries",
    "Gunther Smith",
    "Macie West",
    "Michelle Queen",
    "Shane Miguelito",
    "Fernando Dela Cruz",
    "Akiko Yukihime"
];

let friendsList = [];

/*
    
   1. Create a function called register which will allow us to register into the registeredUsers list.
        - this function should be able to receive a string.
        - determine if the input username already exists in our registeredUsers array.
            -if it is, return the message:
                "Registration failed. Username already exists!"
            -if it is not, add the new username into the registeredUsers array and return the message:
                "Thank you for registering!"
        - invoke and register a new user.
        - outside the function log the registeredUsers array.

*/
function register(userName){
    if (registeredUsers.includes(userName)) {
    return "Registration failed. Username already exists!";
} else {
    registeredUsers.push(userName);
    return "Thank you for registering!"; 
    }
}
console.log(register("Tilting Bird"));
console.log(registeredUsers);

/*
    2. Create a function called addFriend which will allow us to add a registered user into our friends list.
        - this function should be able to receive a string.
        - determine if the input username exists in our registeredUsers array.
            - if it is, add the foundUser in our friendList array.
                    -Then return the message with the following message:
                        - "You have added <registeredUser> as a friend!"
            - if it is not, return the message:
                - "User not found."
        - invoke the function and add a registered user in your friendsList.
        - Outside the function log the friendsList array in the console.

*/
function addFriend(userName){
    if (registeredUsers.includes(userName)){
        console.log("You have added " + (userName) + " as a friend!");
        friendsList.push(userName);
    } else {
        console.log("User not found.");
        friendsList.push(userName);
    }
};
addFriend('Tilting Bird');
console.log(friendsList);

/*
    3. Create a function called displayFriends which will allow us to show/display the items in the friendList one by one on our console.
        - If the friendsList is empty return the message: 
            - "You currently have 0 friends. Add one first."
        - Invoke the function.

*/
function displayFriends(friend){
    if (friendsList === 0){
        console.log("You currently have 0 friends. Add one first and try to have a little bit of empathy");
    } else {
       friendsList.forEach(function(friend){
        console.log(friend);
       })
        
    }
}
displayFriends()

    


/*
    4. Create a function called displayNumberOfFriends which will display the amount of registered users in your friendsList.
        - If the friendsList is empty return the message:
            - "You currently have 0 friends. Add one first."
        - If the friendsList is not empty return the message:
            - "You currently have <numberOfFriends> friends."
        - Invoke the function

*/
let numberOfFriends = friendsList.length
function displayNumberOfFriends(numberOfFriends){
    if (numberOfFriends === 0){ 
    console.log("You current have 0 friends. Add one first");
    } else { 
        console.log("You currently have " + (numberOfFriends) + " friends");
    }
}

displayNumberOfFriends(numberOfFriends);

/*
    5. Create a function called deleteFriend which will delete the last registeredUser you have added in the friendsList.
        - If the friendsList is empty return a message:
            - "You currently have 0 friends. Add one first."
        - Invoke the function.
        - Outside the function log the friendsList array.

*/


function deleteFriend(){
    if (numberOfFriends === 0){
        console.log("You current have 0 friends. Add one first");
    } else {
        friendsList.pop()
    }
}
console.log(friendsList);

/*
    Stretch Goal:

    Instead of only deleting the last registered user in the friendsList delete a specific user instead.
        -You may get the user's index.
        -Then delete the specific user with splice().

*/
function deleteSpecificUser(){
    if (numberOfFriends === 0){
        console.log("You current have 0 friends. Add one first");
    } else { 
        if(friendsList.includes(userName)){
        friendsList.splice(index,1);
    }   else{
        console.log('User not found.');
        }
    }
}





//For exporting to test.js
try {
    module.exports = {
        registeredUsers,
        friendsList,
        register,
        addFriend,
        displayFriends,
        displayNumberOfFriends,
        deleteFriend
    }
}
catch(err) {

}


