console.log("Hello World!")

// Functions
	// FUnctions in JavaScript are lines/blocks of codes that tell our device/application to perform a certain task 
	// Functions are mostly created to create complicated tasks to run several lines of  code in succession 

// Function Declaration
	
	function printName(){ 
		console.log("My name is John");
	};

// Function Invocation 

	printName();

// Function Declarations vs Expressions 

	//  Function Declaration 

	//  A function can be created thought function declaration by using the function keyword and adding a function name 

	declaredFunction ();

	function declaredFunction() {
		console.log("Hello World from delcaredFunction");	
	}

	declaredFunction();

	// Function Expression 
	// A function can also be stored in a variable. This is called function expression

	// A function expression is an anonymous function assigned to the variableFunction

	// Anonymous Function - a function without a name 

	let variableFunction = function(){
		console.log("Hello Again!");
	}

	variableFunction();

	// We can also create a function expression of a named function 
	// Howerever, to invoke the function expression, we invoke it by its variable name, not by its function name 

	let funcExpression = function funcName(){ 
		console.log("Hello from the other side");
	}

	funcExpression();

	// You can reassign declared functions and function expressions to new anonymous functions 

	declaredFunction = function(){
		console.log("updated declaredFunction")
	}

	declaredFunction();

	funcExpression = function(){
		console.log("Updated funcExpression");	
	}

	funcExpression();

	// However, we cannot reassign a function expression initialized with const 

	 const constantFunc = function(){ 
	 	console.log("Initialized with const!");
	 }

	 constantFunc();

	/*constantFunct = function(){
		console.log("Cannot be reassigned!"); 
	}

	constantFunct();*/

// Function Scoping 

/*
	Scope is the accessibility (visibitility) of variables within our program 

	JavaScript Variables has 3 types of scope: 
	1. local/block scope 
	2. global scope 
	3. function scope
*/

	{
		let localVar = "Armando Perez";
	}
	

	let globalVar = "Mr.Worldwide";
	console.log(globalVar);

	// Function Scope 

	function showNames(){
		var functionVar = "Joe";
		const functionConst = "John";
		let functionLet = "Jane"; 

		console.log(functionVar);
		console.log(functionConst);
		console.log(functionLet);
	}

	showNames(); 

	// console.log(functionVar);
	// console.log(functionConst);
	// console.log(functionLet);

	// Nested Functions 

		// You can create another function inside a function. This is called a nested function. 

		function myNewFunction(){
			let name = "Jane";

			function nestedFunction() {
				console.log(name);
			}
			nestedFunction();
		}

		myNewFunction();

	// Function and Global Scoped Variables 
		// Global Scoped Varaibles
		let globalName = "Alexandro";

		function myNewFunction2(){
			let nameInside = "Renz";

			console.log(globalName);
		};

		myNewFunction2();

		// console.log(nameInside);

	// Using alert()

		// alert() allows us to show a small window at the top of our browser page to show information to our users. As opposed to console.log()

		alert("Hello World!");

		function showSampleAlert(){
			alert("Hello, User");
		}; 
		showSampleAlert();

		console.log("I will only log in the console when the alert is dismissed")

	// Using prompt()
	// prompt() allows us to shaw a small window at the browser to gather user input. It, much like alert() will have the page wait until it is dismissed 

	let samplePrompt = prompt("Enter your name: ");
	
	console.log("Hello, " + samplePrompt);	

	let sampleNullPrompt = prompt("Don't enter anything");

	console.log(sampleNullPrompt);
	// Returns an empty string when there is no input. Or null if the user cancels the prompt().

	function printWelcomeMessage(){
		let firstName = prompt("Enter your first name: ");
		let lastName = prompt("Enter youlr last name: ");

		console.log("Hello " + firstName + " " + lastName + "!");
		console.log("Welcome to my page");
	}

	printWelcomeMessage()

	// Function Naming Conventions
	// Function names should be definitive of the task it will perform. It usually contains a verb

	function getCourses() {
		let courses = ["Science 101", "Math 101", "English 101"];
		console.log(courses);
	}

	getCourses();

	// Avoid generic names to avoid confusion 

	function get(){
		let name = "Jamie";
		console.log(name);
	}

	get();

	// Avoid pointless and inappropriate function names

	function foo() {
		console.log(25%5);
	};

	foo();

	// Name your functions in small caps. Follow camelCase when naming variables and functions 

	function displayCarInfo(){
		console.log("Brand: Toyota");
		console.log("Type: Sedan");
		console.log("Price: 1,500,000")
	}

	displayCarInfo();