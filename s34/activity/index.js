const express = require("express");

const app = express();

const port = 3000;

app.use(express.json());

app.use(express.urlencoded({ extended: true }));

let users = [
  { userName: "John Doe", password: "johndoe123" },
  { userName: "John Smith", password: "johnsmith123" },
  { userName: "Johnny Alegre", password: "johnnyalegre123" },
];

app.get("/home", (req, res) => {
  res.send("Welcome to the home page");
});

app.get("/users", (req, res) => {
  res.send(users);
});

app.delete("/delete-user", (req, res) => {
  let message;
  for (let i = 0; i < users.length; i++) {
    if (req.body.userName == users[i].userName) {
      users.splice(i, 1);
      message = `User ${req.body.userName} has been deleted`;
      break;
    } else {
      message = "User does not exist.";
    }
  }
  res.send(message);
});

if (require.main === module) {
  app.listen(port, () => console.log(`Server running at ${port}`));
}

module.exports = app;
