// 2. 

db.fruits.countDocuments({ onSale: true });

//  3. 

db.fruits.countDocuments({ stock: {$gte : 20} });

//  4.

db.fruits.aggregate([
  { $match: { onSale: true } },
  { $group: { _id: "$supplier", avgPrice: { $avg: "$price" } } }
]);

//  5. 

db.fruits.aggregate([
  { $group: { _id: "$supplier_id", maxPrice: { $max: "$price" } } }
]);

// 6. 

db.fruits.aggregate([
  { $group: { _id: "$supplier_id", minPrice: { $min: "$price" } } }
]);