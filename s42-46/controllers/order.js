const Order = require("../models/Order");
const bcrypt = require("bcrypt");



// Checkout
module.exports.checkout = (userId, cart) => {
  // console.log(cart.products[0].productId);
  // console.log(userId)
  // console.log(cart)

  // return Order.findById(userId).then(order => {
    let newOrder = new Order({
      userId: userId,
      products: cart.products,
      totalAmount: cart.totalAmount
    })

    // console.log(newOrder)
    return newOrder.save().then((order, error) => {
      if(error) {
        return false;
      }else {
        return true;
      }
    });

  // })
};


// Retrieve authenticated user's orders

module.exports.getOrders = (userId) => {
	return Order.find({userId: userId}).then(user => {
		if(user === null){
			return false;
		} else { 
			return user; 
		}
	})
};

module.exports.getAllOrders = (userId, cart) => { 
	return Order.find({productId: productId}).then(user => {
		if(user === null){
			return false;
		} else { 
			return user; 
		}
	})
};

module.exports.getSubtotal = async (cart) => {
  try {
    let productsWithSubtotal = [];
    for (let i = 0; i < cart.products.length; i++) {
      let product = await Product.findById(cart.products[i].productId);
      let subtotal = product.price * cart.products[i].quantity;
      productsWithSubtotal.push({
        productId: product._id,
        name: product.name,
        price: product.price,
        quantity: cart.products[i].quantity,
        subtotal: subtotal
      });
    }
    
    return productsWithSubtotal;
  } catch (error) {
    console.error(error);
    return null;
  }
};

module.exports.getTotal = async (userId, cart) => {
  try {
    let productsWithSubtotal = [];
    for (let i = 0; i < cart.products.length; i++) {
      let product = await Product.findById(cart.products[i].productId);
      let subtotal = product.price * cart.products[i].quantity;
      productsWithSubtotal.push({
        productId: product._id,
        name: product.name,
        price: product.price,
        quantity: cart.products[i].quantity,
        subtotal: subtotal
      });
    }
    
    let totalAmount = productsWithSubtotal.reduce((acc, curr) => acc + curr.subtotal, 0);

    let newOrder = new Order({
      userId: userId,
      products: productsWithSubtotal,
      totalAmount: totalAmount
    });

    let savedOrder = await newOrder.save();

    let order = await Order.findById(savedOrder._id).populate('products.productId');

    return order;

  } catch (error) {
    console.error(error);
    return null;
  }
};