const http = require('http');


const port = 3000; 


const server = http.createServer((request, response) => {
	if(request.url == '/login'){
		response.writeHead(200, {'Content-Type': 'text/plain'})
		response.end('Hi, you are at the login page')
	} 
	else {
		response.writeHead(404, {'Content-Type': 'text/plain'})
		response.end('Error, this page is not available')
	}
});


server.listen(3000, () => {
  console.log('Server running at http://localhost:3000/');
});