const User = require("../models/User");
const Product = require("../models/Product");
const bcrypt = require("bcrypt");
const auth = require("../auth");

module.exports.checkEmailExists = (reqBody) => {
	// The result is sent back to the frontend via the "then" method found in the route file 
	return User.find({email : reqBody.email}).then(result => {
		// The "find" method returns a record if a match is found
		if(result.length > 0){
			return true;
		// No duplicate email found
			// The user is not yet registered in the database
		} else {
			return false;
		}
	})
};


module.exports.registerUser = (reqBody) => {
	// Creates a variable "newUser" and instantiates a new "User" object using the mongoose model
	// Uses the information from the request body to provide all the necessary information
	let newUser = new User({
		firstName: reqBody.firstName,
		lastName: reqBody.lastName,
		email: reqBody.email,
		mobileNo: reqBody.mobileNo,
		password : bcrypt.hashSync(reqBody.password, 10)
	})

	// Saves the created object to our database
	return newUser.save().then((user, error) =>{
		// User registration failed
		if(error) {
			return false;
		} else {
			return true
		};
	})


};

module.exports.loginUser = (reqBody) => {
	// The "findOne" method returns the first record in the collection that matches the search criteria
	// We use the "findOne" method instead of the "find" method which returns all records that match the same criteria
	return User.findOne({email : reqBody.email}).then(result => {
		// User does not exist
		if(result == null){
			return false;
		// User exists
		} else {
			// Creates the variable "isPasswordCorrect" to return the result of the comparing login form password and the database password
			// The "compareSync" method is used to compare a non encrypted password from the login from to the encrypted password retrieved from the database
			const isPasswordCorrect = bcrypt.compareSync(reqBody.password, result.password)

			// If the passwords match/result of the above code is true
			if(isPasswordCorrect) {
				// Generate an access token
				// Uses the "createAccessToken" method defined in the "auth.js" file
				// Returning an object back to the frontend application is common practice to ensure information is properly labeled
				return {access : auth.createAccessToken(result)}
			// Passwords do not match
			} else {
				return false;
			}
		}
	})
};

// Non Admin user checkout
module.exports.userCheckout = async(data)=>{
    // Add the productID in the userOrders array of the user
    // Creates an "isUserUpdated" variable and returns upon successful update of otherwise false
    // Using the

    let isUserUpdated = await User.findById(data.userId).then(user=>{

        return user.save().then((user,error)=>{
            if (error){
                return false
            }else{
                return true
            }
        })
    })
     // Add the userID in the ordered product array of the product
     // Using the 'await' keyword will allow the checkout method to complete updating the product before returning a response back to the front end // 

     let isProductUpdated = await Product.findById(data.productId).then(product=>{
         // Adds the userId in the user's ordered array
         product.userOrders.push({userId: data.userId});
				 // Saves the updated product information in the database
         return product.save().then((product,error)=>{
             if(error){
                 return false
             }else{
                 return true
             }
         })
     })

     // Condition that willl check if the user and Course documents have been updated
     // User checkout successful

     if(isUserUpdated && isProductUpdated){
         return true

         // user checkout failure
     }else{
         return false
     }
};

// Retrieve User details
module.exports.getProfile = (data) => {

	return User.findById(data.userId).then(result => {

		// Changes the value of the user's password to an empty string when returned to the frontend
		// Not doing so will expose the user's password which will also not be needed in other parts of our application
		// Unlike in the "register" method, we do not need to call the mongoose "save" method on the model because we will not be changing the password of the user in the database but only the information that we will be sending back to the frontend application
		if(result == null){
			return false
		} else {
			result.password = "";

			// Returns the user information with the password as an empty string
			return result;
		}

	});

};

module.exports.setAsAdmin = (userId) => {
	return User.findById(userId).then(user => {
		if(user === null){
			return false
		}else {
			user.isAdmin = true;
			return user.save().then((updated, err) => {
				if(err){
					return false;
				}else {
					return true;
				}
			})
		}
	})
};

// View Cart
module.exports.viewCart = (userData) => {

	// console.log(userData)
	return User.findById(userData.id).then(result => {
		if(result) {
			return result;
		}else {
			return false
		}
	})
}

module.exports.addToCart = (userData, data) => {

	console.log(data.productsId)

	return Product.findById(data.productId).then(result => {
		return User.findById(userData.id).then(user => {
			if(user === null) {
				return false
			}else {
				// console.log(result.quantity)
				user.cart.unshift(
				{
					productName: result.productName,
					productId: data.productId,
					price: result.price,
					quantity: data.quantity
				});
				console.log(user.cart)
				const currentOrder = user.cart[0]
				// console.log(result.orders)
				return user.save().then((updatedUser, error) => {
					if(error) {
						return false
					}else {
						const currentOrder = updatedUser.cart[0];

						result.orders.unshift(
						{
							orderId: currentOrder._id,
							_id: null
						})
						result.save()

						return true
					}
				});
			}
		})
	})
};

module.exports.removeFromCart = (userData, reqBody) => { 

	return User.findById(userData.id).then(user => {

		const orderId = user.cart[reqBody.index].id 
		user.cart.splice((reqBody.index), 1)
		console.log(user.cart)

		return user.save() 
	})
};

module.exports.editCart = (userData, reqBody) => {

	console.log(reqBody)

	return User.findById(userData.id).then(user => {

		user.cart[reqBody.index].quantity = reqBody.quantity
		console.log(user.cart[reqBody.index])
		return user.save()
	})
};

module.exports.removeAllFromCart = (userData) => {

	return User.findById(userData.id).then(user => {
		console.log(user.cart)
		user.cart = []
		console.log(user.cart)
		return user.save().then(data => {
			if(data) {
				return true
			} else { 
				return false
			}
		})
	})
};


