const express = require("express");
const mongoose = require("mongoose");
const app = express();
const port = 3001;

mongoose.connect("mongodb+srv://francis-255:admin123@zuitt-bootcamp.xmfelda.mongodb.net/?retryWrites=true&w=majority", 
	{
		useNewUrlParser : true,
		useUnifiedTopology: true
	}
);

const db = mongoose.connection;

db.on("error", console.error.bind(console, "connection error"));

db.once("open", () => console.log("We're connected to the cloud database"))

const taskSchema = new mongoose.Schema({
	userName: String,
	password: String	 
})

const Task = mongoose.model("Task", taskSchema);

app.use(express.json());

app.use(express.urlencoded({extended:true}));

app.post("/signup", (req, res)=> {
  if(!req.body.userName || !req.body.password){
    return res.status(400).send("Both username and password are required.");
  }
	Task.findOne({name : req.body.userName}).then((result, err) => {
		
		if(result != null && result.userName == req.body.password){	
			return res.send("Duplicate username found");	
		} else {
			let newUser = new Task({
				userName : req.body.userName,
				password : req.body.password
			});
			newUser.save().then((savedTask, saveErr) => {
				if(saveErr){
					return console.error(saveErr);
				} else {
					return res.status(201).send("New user registered");
				}
			})
		}
	})
});

app.get("/signup", (req, res) => {

		Task.find({}).then((result, err) => {

			if(err) {
				return console.log(err)
			} else {
				return res.status(200).json({
					data:result
				})
			}
		})
})

if(require.main === module){
	app.listen(port, () => console.log(`Server running at port ${port}`));
}

module.exports = app;