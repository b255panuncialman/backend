// Enroll user to a class 
/*
	Steps: 
	1. Find the document in the database using the user's ID
	2. Add the course ID to the user's enrollment array 
	3. Update the document in the MongoDB Atlas Database 
*/
// Async await will be used in enrolling the user because we will need to update 2 separate documents when enrolling a user 

module.exports.enroll = async (data) => {
	// Add the course ID in the enrollments array of the user 
	// Creates an "isUserUpdated" variable and returns true upon successful update otherwise false
	// Using the "await" keyword will allow the enroll method to complete updating before returning a response back to the frontend 
	let isUserUpdated = await User.findById(data.userId).then(user => {

		// Adds the courseId in the user's enrollments array
		return user.save().then((user, error) => {
			if(error){
				return false;
			} else {
				return true;
			}
		})
	})
	// Add the user ID in the enrollees array of the course
	// Using the "await" keyword will allow the enroll method to complete updating the course before returning a response back to the frontend 
	let isCourseUpdated = await Course.findById(data.courseId).then(course => {

		// Adds the userId in the course's enrollees array 
		course.enrollees.push({userId : data.userId});

		// Saves the updated course information in the database 
		return course.save().then((course, error) => { 
			if(error){
				return false;
			} else {
				return true; 
			}
		})
	})
	// Condition that will check if the user and course documents have been updated 
	// User enrollment successful 
	if(isUserUpdated && isCourseUpdated){
		return true;
	// User enrollment successful 
	} else {
		return false;
	}

};