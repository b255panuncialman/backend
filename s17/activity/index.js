/*
	//Note: strictly follow the variable names and function names.

	1. Create a function named printUserInfo() which is able to display a user's to fullname, age, location and other information.
		-invoke the function to display information similar to the expected output in the console.
		-check your spelling
*/
	//first function here:
	
	function printUserInfo(){
		let userName = "Jolly Spaghetti";
		let userAge = 37;
		let userLocation = "456 Farmers Market, Cubao, Quezon City"
		let userDog = "Tempra"
		let userCat = "Mrs. Scratchie"

		console.log("Hi, I'm : " + userName);
		console.log("I am " + userAge + " years old");
		console.log("I live at " + userLocation);
		console.log("I have a dog named " + userDog + ".");
		console.log("I have a cat named " + userCat + " : ). ");
	}
	console.log("printUserInfo");
	printUserInfo();



/*
	2. Create a function named printFiveBands which is able to display 5 bands/musical artists.
		-invoke the function to display information similar to the expected output in the console.
		-check your spelling
	
*/
	//second function here:

	function printFiveBands(){
		let band1 = "Animal Collective";    
		let band2 =	"Dave Matthews Band";
		let band3 =	"Red Hot Chilli Peppers";
		let band4 =	"Foo Fighters";
		let band5 =	"Westlife";
		console.log(band1)
		console.log(band2)
		console.log(band3)
		console.log(band4)
		console.log(band5)
	}
	console.log("printFiveBands");
	printFiveBands();


/*
	3. Create a function named printFiveMovies which is able to display the name of 5 movies.
		-invoke the function to display information similar to the expected output in the console.
		-check your spelling
	
*/
	
	//third function here:

	function printFiveMovies(){
		let movie1 = "The Shawshank Redemption";    
		let movie2 = "City of God";
		let movie3 = "Terribly Happy";
		let movie4 = "The Notebook";
		let movie5 = "Superbad";
		console.log(movie1)
		console.log(movie2)
		console.log(movie3)
		console.log(movie4)
		console.log(movie5)
	}
	console.log("printFiveMovies");
	printFiveMovies();

/*
	4. Debugging Practice - Debug the following codes and functions to avoid errors.
		-check the variable names
		-check the variable scope
		-check function invocation/declaration
		-comment out unusable codes.
		-check your spelling

		-invoke the function to display information similar to the expected output in the console.
*/

function printFriends(){
	let friend1 = "Eugene"; 
	let friend2 = "Dennis"; 
	let friend3 = "Vincent";

	console.log("These are my friends:");
	console.log(friend1); 
	console.log(friend2); 
	console.log(friend3); 
};

console.log("printFriends");
printFriends();


//Do not modify
//For exporting to test.js
try{
	module.exports = {
		printUserInfo,
		printFiveBands,
		printFiveMovies,
		printFriends
	}
} catch(err){

}
