// console.log("Hello World");

//Follow the property names and spelling given in the google slide instructions.

// Create an object called trainer using object literals

let trainer = {
	name: 'Tilting Bird', 
	age: '17',
	pokemon: ['Charizard', 'Mewtwo', 'Snorlax', 'Gyarados'],
	friends: {
		kanto: [ "Brock","Misty"],
		honnen: ["May", "Max"] 
	},
	talk: function(){
		console.log(this.pokemon[2] + ", I choose you!");
	},
}
console.log(trainer);

// Initialize/add the given object properties and methods

// Properties

// Methods
console.log('Result of dot notation: ');
console.log(trainer.name);
// Check if all properties and methods were properly added
console.log('Result of square bracket notation: ')
console.log(trainer['pokemon']);
// Access object properties using dot notation
console.log('Result of talk method: ');
trainer.talk();

// Access object properties using square bracket notation

// Access the trainer "talk" method


// Create a constructor function called Pokemon for creating a pokemon
function Pokemon(name, level){
	// Properties 
	this.name = name; 
	this.level = level; 
	this.health = 2*level;
	this.attack = level;

	// Methods 
	this.tackle = function(target){
		console.log(this.name + ' tackled ' + target.name);
		console.log(target.name + "'s health is reduced to: " + (target.health - this.attack));
		if (target.health - this.attack <= 0) {
			console.log('Enemy ' + target.name + " has fainted!");
		} else {
			console.log('Enemy ' + target.name + " still has some fight in it!");
		}
	};
	this.faint = function(){
		console.log(this.name + ' fainted');
	}
};

// Create/instantiate a new pokemon
let pikachu = new Pokemon("Pikachu", 10);
console.log(pikachu);

// Create/instantiate a new pokemon
let articuno = new Pokemon("Articuno", 25);
console.log(articuno);
// Create/instantiate a new pokemon
let machamp = new Pokemon("Machamp", 20);
console.log(machamp);
// Invoke the tackle method and target a different object
machamp.tackle(articuno);

// Invoke the tackle method and target a different object
articuno.tackle(pikachu);







//Do not modify
//For exporting to test.js
try{
	module.exports = {
		trainer,
		Pokemon 
	}
} catch(err) {

}
