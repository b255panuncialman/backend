
// #3
fetch('https://jsonplaceholder.typicode.com/posts/')
.then((response) => response.json())
.then((json) => console.log(json));

// #4 


fetch('https://jsonplaceholder.typicode.com/posts/')
  .then(response => response.json())
  .then(data => {
    let posts = data.map(post => ({
      id: post.id,
      title: post.title,
      body: post.body,
      userId: post.userId
    }));
    console.log(posts);
  })

// #5 

fetch('https://jsonplaceholder.typicode.com/posts/92')
.then((response) => response.json())
.then((json) => console.log(json));

// #6


// #7 

fetch('https://jsonplaceholder.typicode.com/posts/1')
.then((response) => response.json())
.then((json) => console.log(json));


fetch('https://jsonplaceholder.typicode.com/posts', {
	
	method: 'POST',

	headers: { 
		'Content-Type': 'application/json',
	},
	
	body: JSON.stringify({ 
		title: 'New Post',
		body: 'FDF Labs',
		userId: 1
	})
})
.then((response) => response.json())
.then((json) => console.log(json));

// #8 

fetch('https://jsonplaceholder.typicode.com/posts/33', {
	method: 'PUT',

	headers: { 
		'Content-Type': 'application/json',
	},

	body: JSON.stringify({ 
		id: 33,
		title: 'Updated Post',
		body: 'Hello Again',
		userId: 33
	})
})
.then((response) => response.json())
.then((json) => console.log(json));

// #9 

fetch('https://jsonplaceholder.typicode.com/posts/33', {
	method: 'PUT',

	headers: { 
		'Content-Type': 'application/json',
	},

	body: JSON.stringify({ 
		title: 'Updated Item',
		description: 'FDF Labs.20',
		status: "Work in progress",
		dateCompleted: "03/26/23",
		userId: 33
	})
})
.then((response) => response.json())
.then((json) => console.log(json));

// #10

fetch('https://jsonplaceholder.typicode.com/posts/33', {
	method: 'PUT',

	headers: {
		'Content-Type': 'application/json',
	},

	body: JSON.stringify({ 
		status: '90% complete'
	})
})
.then((response) => response.json())
.then((json) => console.log(json));

// #11
fetch('https://jsonplaceholder.typicode.com/posts/33', {
	method: 'PUT',

	headers: {
		'Content-Type': 'application/json',
	},

	body: JSON.stringify({ 
		status: 'Complete',
		dateCompleted: "03/27/23"
	})
})
.then((response) => response.json())
.then((json) => console.log(json));

// #12
fetch('https://jsonplaceholder.typicode.com/posts/33', {
	method: 'DELETE',
});

