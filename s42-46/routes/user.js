const express = require("express");
const router = express.Router();
const userController = require("../controllers/user");
const auth = require('../auth.js');

// Route for check email
router.post("/checkEmail", (req, res) => {
	userController.checkEmailExists(req.body).then(resultFromController => res.send(resultFromController))
});

// Route for user registration
router.post("/register", (req, res) => {
	userController.registerUser(req.body).then(resultFromController => res.send(resultFromController))
});

// Route for user authentication
router.post("/login", (req, res) => {
	userController.loginUser(req.body).then(resultFromController => res.send(resultFromController))
});

// Route for getting user details 
router.get("/details", auth.verify, (req, res) => {
	// Uses the "decode" method defined in the "auth.js" file to retrieve user information from the token passing the "token" from the request header as an argument
	const userData = auth.decode(req.headers.authorization);
	// Provides the user's ID for the getProfile controller method
	userController.getProfile({userId : req.body.id}).then(resultFromController => res.send(resultFromController));
});

// Route for non-admin user check out 
router.post("/checkout", (req,res) =>{
    const userData = auth.decode(req.headers.authorization);
    if(userData.isAdmin == false){
        console.log(req.body)
    let data = {
    	userId: req.body.userId,
        productId:req.body.productId
    }
    userController.userCheckout(data).then(resultFromController => res.send(resultFromController));
    } else{
        res.send(false);
    }

})

// Route for setting user as admin 
router.put("/:userId/setAsAdmin", auth.verify, (req,res) => {
	if(auth.decode(req.headers.authorization).isAdmin === false) {
		res.send(false);
	}else {
		userController.setAsAdmin(req.params.userId).then(resultFromController => res.send(resultFromController));
	}
});
// Route for add to cart
router.post("/addToCart", auth.verify, (req, res) => {
	let userData = auth.decode(req.headers.authorization) 

	userController.addToCart(userData, req.body).then(resultFromController => res.send(resultFromController));

});

// Route for remove item from cart
router.delete("/remove", auth.verify, (req, res) => {
	let userData = auth.decode(req.headers.authorization) 

	userController.removeFromCart(userData, req.body).then(resultFromController => res.send(resultFromController));
});

// Route for editing quantity from cart
router.put("/edit", auth.verify, (req, res) => {
	let userData = auth.decode(req.headers.authorization) 

	userController.editCart(userData, req.body).then(resultFromController => res.send(resultFromController));
});

// Route for remove all items from cart
router.put("/removeAll", auth.verify, (req, res) => {
	let userData = auth.decode(req.headers.authorization) 

	userController.removeAllFromCart(userData).then(resultFromController => res.send(resultFromController));
});

// Allows us to export the "router" object that will be acessed in our "index.js" file
module.exports = router;



