const express = require("express");
const mongoose = require("mongoose");
const cors = require("cors");

// Allows us to access Routes
const userRoutes = require("./routes/user");
const productRoutes = require("./routes/product");
const orderRoutes = require("./routes/order");


// Creates an "app" variable that stores the result of the "express" function that initializes our express application
const app = express();


// Connect to our MongoDB Database 
mongoose.connect(
	"mongodb+srv://francis-255:admin123@zuitt-bootcamp.xmfelda.mongodb.net/?retryWrites=true&w=majority", {
			useNewUrlParser: true, 
			useUnifiedTopology: true
});

mongoose.connection.once('open', () => console.log('Now connected to MongoDB Atlas'));

app.use(cors());
app.use(express.json());
app.use(express.urlencoded({extended: true}));
// Defines the "/users" string to be included for all user routes defined in the "user" route file 
app.use("/users", userRoutes);
// Defines the "/products" string to be included for all product routes defined in the "product" route file
app.use("/products", productRoutes);
// Defines the "/order" string to be included for all order routes defined in the "order" route file
app.use("/orders", orderRoutes);


// if(require.main) would allow us to listen to the app directly if it is not imported to another module, it will run the app directly 
// else, if it is needed to be imported, it will not run the app and instead export it be used in another file

if(require.main === module){
	// Will use the defined port number for the application whenever an environment variable is available or will use port 4000 if none is defined 
// This syntax will allow flexibility when using the application locally or as a hosted application
	app.listen(process.envPORT || 4000, () => {
		console.log(`API is now online on port ${ process.env.PORT || 4000}`)
	});
}

module.exports = app; 